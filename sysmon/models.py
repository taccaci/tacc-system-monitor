from __future__ import unicode_literals

from django.db import models
from cms.models.pluginmodel import CMSPlugin
from django.conf import settings

class SingleSystemPluginModel(CMSPlugin):
    systems = models.CharField(max_length=512,blank=True, default='frontera.tacc.utexas.edu',\
        help_text='Single system i.e. "frontera.tacc.utexas.edu"')

class MultiSystemPluginModel(CMSPlugin):
    sysmon_default_systems = getattr(settings, 'SYSMON_DEFAULT_SYSTEMS',\
            'frontera.tacc.utexas.edu\n' +\
            'stampede2.tacc.utexas.edu\n' +\
            'lonestar5.tacc.utexas.edu\n' +\
            'hikari.tacc.utexas.edu\n' +\
            'wrangler.tacc.utexas.edu\n' +\
            'maverick2.tacc.utexas.edu\n' +\
            'rustler.tacc.utexas.edu\n')
    
    systems = models.TextField(blank=True, help_text='System names, one per line i.e. frontera.tacc.utexas.edu',\
        default=getattr(settings, 'SYSMON_DEFAULT_SYSTEMS', sysmon_default_systems))
