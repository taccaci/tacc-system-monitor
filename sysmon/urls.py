
from django.conf.urls import url
from sysmon import views

urlpatterns = [
    url(
        r'^',
        views.SysMonMultiView.as_view(),
        name='sysmon'
    ),
]
