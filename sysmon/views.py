from django.shortcuts import render
from django.views.generic import TemplateView
from django.conf import settings
from models import MultiSystemPluginModel
from datetime import datetime, timedelta
import dateutil.parser
import requests
import json
import logging
import pytz

logger = logging.getLogger(__name__)

'''
    Pulls and parses data from TACC User Portal then populates and returns a list of Systems objects
'''
def get_sysmon_data(requested_systems=''):
    systems = []
    requested_systems = requested_systems.splitlines()
    system_status_endpoint = getattr(settings,\
        'SYSMON_URL','https://portal.tacc.utexas.edu/commnq/index.json')
    systems_json = requests.get(system_status_endpoint).json()
    for sys in systems_json:
        try:
            system = System(systems_json.get(sys))
            if system.hostname in requested_systems:
                systems.append(system)
        except Exception as exc:
            logger.error(exc)
    systems = sorted(systems, key=lambda s: s.cpu_count, reverse=True)
    return systems

class SysMonMultiView(TemplateView):
    template_name = "sysmon/multi_display.html"
    def get_context_data(self, **kwargs):
        context = super(SysMonMultiView, self).get_context_data(**kwargs)
        system_statuses = get_sysmon_data(\
            requested_systems=MultiSystemPluginModel.sysmon_default_systems)
        context['statuses'] = system_statuses
        return context

class System:
    def __init__(self, system_dict):
        try:
            self.hostname = system_dict.get('hostname')
            self.display_name = system_dict.get('displayName')
            if 'ssh' in system_dict.keys():
                self.ssh = system_dict.get('ssh')
            if 'heartbeat' in system_dict.keys():
                self.heartbeat = system_dict.get('heartbeat')
            if 'tests' in system_dict.keys():
                self.status_tests = system_dict.get('tests')
            if 'jobs' in system_dict.keys():
                self.resource_type = 'compute'
                self.jobs = system_dict.get('jobs')
                self.load_percentage = system_dict.get('load')
                if isinstance(self.load_percentage, float) or \
                    isinstance(self.load_percentage, int):
                        self.load_percentage = int(( self.load_percentage * 100))
                else:
                    self.load_percentage = None
                self.cpu_count = system_dict.get('totalCpu')
                self.cpu_used = system_dict.get('usedCpu')
            else:
                self.resource_type = 'storage'
                self.cpu_count = 0
            self.is_operational = self.is_up()
        except Exception as exc:
            logger.error(exc)
    '''
    Checks each uptime metric to determine if the system is available
    '''
    def is_up(self):
        if self.resource_type == 'compute':
            if not self.load_percentage or not self.jobs:
                    return False
            if self.load_percentage > 99 and self.jobs.get('running', 0) < 1:
                return False
        #let's check each test:
        for st in self.status_tests:
            test = self.status_tests.get(st)
            if not test.get('status'):
                return False
             # now, let's check that the status has been updated recently
            if not self.status_updated_recently(last_updated=test.get('timestamp')):
                return False
        return True
       
    '''
    Checks whether system availability metrics are being updated regularly
    '''
    def status_updated_recently(self,last_updated=None):
        if not last_updated:
            return False
        last_updated = dateutil.parser.parse(last_updated)
        current_time = datetime.now()
        ## if we want to change the update interval test, we can do it below
        expire_time = last_updated + timedelta(minutes=10)
        return pytz.UTC.localize(current_time) < expire_time




